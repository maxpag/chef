# /*
     g++ -Wall -Wextra -g -std=gnu++2a -fsanitize=address,undefined \
        -I../PlatformAgnostic  \
        -I../PlatformSpecific/generic_linux \
        -I../PlatformSpecific/x86_generic \
        -DBUILD_RELEASE \
        -DChefBase_BRIEF_ASSERT \
        ../PlatformAgnostic/ChefBase/assert.cc "$0"
     ./a.out "$@"
     return
# */
/**
 * @addtogroup ChefTest
 * @{
 * @file Test/ChefBase/assertTest.cc
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2022-06-03
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define __noreturn__            // hopefully this clears the attribute away
#define __builtin_unreachable() // hopefully this removes the assertion unreachable
#include <ChefTest/Framework.hh>
#include <ChefBase/assert.hh>

ChefTest_TEST_SUITE( "ChefBase/assert")

namespace
{
    uintptr_t g_pc;
    uintptr_t g_sp;
    bool g_callbackCalled;

    void resetCallbackData() noexcept
    {
        g_pc = 0;
        g_sp = 0;
        g_callbackCalled = false;
    }

    void mockBriefCallback(uintptr_t pc, uintptr_t sp) noexcept
    {
        g_pc = pc;
        g_sp = sp;
        g_callbackCalled = true;
    }
}

namespace ChefSystem
{
    bool g_abortHasBeenCalled = false;

    void resetAbortData()
    {
        g_abortHasBeenCalled = false;
    }

    void abort() noexcept
    {
        g_abortHasBeenCalled = true;
    }
}

ChefTest_TEST_CASE( "assert" )
{
    ChefBase::Assert::setCallback( mockBriefCallback );
    resetCallbackData();
    ChefSystem::resetAbortData();
    ChefBase_ASSERT( true );
    ChefTest_ASSERT( !g_callbackCalled );
    ChefTest_ASSERT( !ChefSystem::g_abortHasBeenCalled );

    ChefBase_ASSERT( false );
    ChefTest_ASSERT( g_callbackCalled );
    ChefTest_ASSERT( g_pc != 0 );
    ChefTest_ASSERT( g_sp != 0 );
    ChefTest_ASSERT( ChefSystem::g_abortHasBeenCalled );
}