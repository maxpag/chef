# /*
     g++ -Wall -Wextra -I../PlatformAgnostic -g -std=gnu++20 -fsanitize=address,undefined \
        ChefBase/ArithmeticSmartTypeBaseTest.cc
     ./a.out "$@"
     return
# */    
/**
 * @addtogroup ChefTest
 * @{
 * @file ../ChefTest/ArithmeticSmartTypeBaseTest.cc
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2014-11-12
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefBase/ArithmeticSmartTypeBase.hh>
#include <ChefTest/Framework.hh>

using namespace ChefBase;

ChefTest_TEST_SUITE( "ChefBase/ArithmeticSmartTypeBase")

class Speed : public ChefBase::ArithmeticSmartTypeBase<Speed,float>
{
    public:
        static constexpr Speed fromMetersPerSecond( float mps ) noexcept;
        constexpr float toMetersPerSecond() const noexcept;
        constexpr float toKilometersPerHour() const noexcept;
    private:
        constexpr Speed( float value ) noexcept;

        // friendship is needed so that the base type could call this class
        // constructor
        friend ChefBase::ArithmeticSmartTypeBase<Speed,float>;
};

constexpr Speed::Speed( float value ) noexcept
    : ChefBase::ArithmeticSmartTypeBase<Speed,float>{ value }
{
}

constexpr Speed Speed::fromMetersPerSecond( float mps ) noexcept
{
    return Speed{ mps };
}

constexpr float Speed::toMetersPerSecond() const noexcept
{
    return getValue();
}

constexpr float Speed::toKilometersPerHour() const noexcept
{
    return getValue()*(3600.0f/1000.0f);
}

#if 0
class Force : public ChefBase::ArithmeticSmartTypeBase<Force,float>
{
    public:
        static constexpr Force fromNewtons( float newtons ) noexcept;
        constexpr float toNewtons() const noexcept;
    private:
        constexpr Force( float value ) noexcept;

        // friendship is needed so that the base type could call this class
        // constructor
        friend ChefBase::ArithmeticSmartTypeBase<Force,float>;
};
#endif

ChefTest_TEST_CASE( "Base" )
{
    Speed s = Speed::fromMetersPerSecond( 2.0f );
    ChefTest_ASSERT( s.toMetersPerSecond() == 2.0f );
    ChefTest_ASSERT( s.toKilometersPerHour() == 7.2f );
}

ChefTest_TEST_CASE( "Relational Operators" )
{
    Speed s0 = Speed::fromMetersPerSecond( 2.0f );
    Speed s1 = Speed::fromMetersPerSecond( 4.0f );
    ChefTest_ASSERT( s0 < s1 );
    ChefTest_ASSERT( s0 <= s1 );
    ChefTest_ASSERT( s0 <= s0 );
    ChefTest_ASSERT( s1 > s0 );
    ChefTest_ASSERT( s1 >= s0 );
    ChefTest_ASSERT( s1 >= s1 );
    ChefTest_ASSERT( s1 != s0 );
    ChefTest_ASSERT( !(s1 != s1) );
    ChefTest_ASSERT( !(s1 == s0) );
    ChefTest_ASSERT( (s1 == s1) );
}

ChefTest_TEST_CASE( "Math Operators" )
{
    Speed s0 = Speed::fromMetersPerSecond( 2.0f );
    Speed s1 = Speed::fromMetersPerSecond( 4.0f );
    ChefTest_ASSERT( s0+s1 == Speed::fromMetersPerSecond( 6.0f ) );
    ChefTest_ASSERT( s0-s1 == Speed::fromMetersPerSecond( -2.0f ) );
    ChefTest_ASSERT( s0*s1 == Speed::fromMetersPerSecond( 8.0f ) );
    ChefTest_ASSERT( s0/s1 == 0.5f );

    ChefTest_ASSERT( s0*4 == 4*s0 );
    ChefTest_ASSERT( s0*4 == Speed::fromMetersPerSecond( 8.0f ));

    ChefTest_ASSERT( s0/4 == Speed::fromMetersPerSecond( 0.5f ));
}

#if 0
ChefTest_TEST_CASE( "Fail to compile" )
{
    Speed s = Speed::fromMetersPerSecond( 2.0f );
    Force f = Force::fromNewtons( 5.0f );
    s+f;
}
#endif

///@}
