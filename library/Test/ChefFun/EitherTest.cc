# /*
g++ -Wall -Wextra -g -std=gnu++17 \
    -DCHEFLIB_X86_TEST \
    -DCHEFLIB_DEBUG_FULL_ASSERT \
    -DBUILD_RELEASE \
    -DBUILD_TEST \
    -I ../PlatformAgnostic \
    ChefFun/EitherTest.cc
./a.out "$@"
return
# */     
/**
 * @addtogroup ChefTest
 * @{
 * @file Test/ChefFun/Either.cc
 * @author Massimiliano Pagani
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFun/Either.hh>
#include <ChefTest/Framework.hh>

ChefTest_TEST_SUITE( "ChefFun/Either" )

using namespace ChefFun;

ChefTest_TEST_CASE( "Left Either" )
{
    auto either = Either<int,std::string>::Left( 3 );
    ChefTest_ASSERT( either.isLeft() );
    ChefTest_ASSERT( !either.isRight() );
    ChefTest_ASSERT( either.getLeft() == 3 );
    bool exceptionThrown = false;
    try
    {
        (void) either.getRight();
    }
    catch( std::bad_variant_access const& )
    {
        exceptionThrown = true;
    }

    ChefTest_ASSERT( exceptionThrown );
}

ChefTest_TEST_CASE( "Right Either" )
{
    std::string abc{"abc"};

    auto either = Either<int,std::string>::Right( abc );
    ChefTest_ASSERT( !either.isLeft() );
    ChefTest_ASSERT( either.isRight() );

    bool exceptionThrown = false;
    try
    {
        (void) either.getLeft();
    }
    catch( std::bad_variant_access const& )
    {
        exceptionThrown = true;
    }
    ChefTest_ASSERT( exceptionThrown );

    ChefTest_ASSERT( either.getRight() == abc );
}

ChefTest_TEST_CASE( "Reverse" )
{
    std::string abc{ "abc" };
    auto eitherL = Either<int,std::string>::Left( 3 );
    auto eitherR = Either<int,std::string>::Right( abc );
    auto rev1 = eitherL.reverse();
    ChefTest_ASSERT( rev1.isRight() );
    ChefTest_ASSERT( rev1.getRight() == 3 );
    auto rev2 = eitherR.reverse();
    ChefTest_ASSERT( rev2.isLeft() );
    ChefTest_ASSERT( rev2.getLeft() == abc );
}

ChefTest_TEST_CASE( "getOrElse/getLeftOrElse" )
{
    std::string RightValue{"abc" };
    constexpr int LeftValue = 3;
    auto eitherL = Either<int,std::string>::Left( LeftValue );
    auto eitherR = Either<int,std::string>::Right( RightValue );

    ChefTest_ASSERT( eitherL.getOrElse( "ok") == "ok" );
    ChefTest_ASSERT( eitherR.getOrElse( "ok" ) == RightValue );

    ChefTest_ASSERT( eitherL.getLeftOrElse( 42 ) == LeftValue );
    ChefTest_ASSERT( eitherR.getLeftOrElse( 42 ) == 42 );
}

ChefTest_TEST_CASE( "functional map" )
{
    std::string abc{ "abc" };
    auto eitherL = Either<int,std::string>::Left( 12 );
    auto eitherR = Either<int,std::string>::Right( abc );

    auto fn = []( std::string const& v ) -> unsigned { return v.size(); };
    auto mapL = eitherL.map( fn );
    auto mapR = eitherR.map( fn );

    ChefTest_ASSERT( mapL.isLeft() );
    ChefTest_ASSERT( mapL.getLeft() == 12 );

    ChefTest_ASSERT( mapR.isRight() );
    ChefTest_ASSERT( mapR.getRight() == abc.size() );
}

ChefTest_TEST_CASE( "functional flatMap" )
{
    std::string abc{ "abc" };
    auto eitherL = Either<int,std::string>::Left( 12 );
    auto eitherR = Either<int,std::string>::Right( abc );

    auto fn = []( std::string const& v ) -> unsigned { return v.size(); };
    auto mapL = eitherL.map( fn );
    auto mapR = eitherR.map( fn );

    ChefTest_ASSERT( mapL.isLeft() );
    ChefTest_ASSERT( mapL.getLeft() == 12 );

    ChefTest_ASSERT( mapR.isRight() );
    ChefTest_ASSERT( mapR.getRight() == abc.size() );
}

ChefTest_TEST_CASE( "fold" )
{
    std::string abc{ "abc" };

    auto eitherL = Either<int,std::string>::Left( 12 );
    auto eitherR = Either<int,std::string>::Right( abc );

    auto foldL = eitherL.fold(
            []( auto&& x ){ return std::to_string( x ); },
            []( auto&& ){ return std::string{"no"}; }
    );
    ChefTest_ASSERT_EQUAL( foldL, "12" );

    auto foldR = eitherR.fold(
            [](auto const& ){ return static_cast<size_t>(0); },
            [](auto const& x){ return x.length();}
    );
    ChefTest_ASSERT_EQUAL( foldR, 3 );

}

ChefTest_TEST_CASE( "merge" )
{
    using Either = Either<int,int>;

    constexpr int RightValue = 2;
    constexpr int LeftValue = 3;

    Either left = Either::Left( LeftValue );
    Either right = Either::Right( RightValue );

    ChefTest_ASSERT_EQUAL( left.merge(), LeftValue );
    ChefTest_ASSERT_EQUAL( right.merge(), RightValue );
}

ChefTest_TEST_CASE( "swap" )
{
    using Either = Either<int,std::string>;

    Either oneL = Either::Left( 32 );
    Either twoL = Either::Left( 12 );
    oneL.swap( twoL );
    ChefTest_ASSERT( oneL.isLeft() );
    ChefTest_ASSERT( twoL.isLeft() );
    ChefTest_ASSERT( oneL.getLeft() == 12 );
    ChefTest_ASSERT( twoL.getLeft() == 32 );

    Either oneR = Either::Right( "abc" );
    Either twoR = Either::Right( "def" );
    oneR.swap( twoR );
    ChefTest_ASSERT( oneR.isRight() );
    ChefTest_ASSERT( twoR.isRight() );
    ChefTest_ASSERT( oneR.getRight() == "def" );
    ChefTest_ASSERT( twoR.getRight() == "abc" );

    Either oneM = Either::Right( "abc" );
    Either twoM = Either::Left( 12 );
    oneM.swap( twoM );
    ChefTest_ASSERT( oneM.isLeft() );
    ChefTest_ASSERT( twoM.isRight() );
    ChefTest_ASSERT( oneM.getLeft() == 12 );
    ChefTest_ASSERT( twoM.getRight() == "abc" );

}

ChefTest_TEST_CASE( "foreach" )
{
    using Either = Either<int,std::string>;

    Either left = Either::Left(12);
    Either right = Either::Right("abc");

    int count = 0;
    left.foreach( [&count]( auto const& ){ ++count; });
    ChefTest_ASSERT( count == 0 );

    right.foreach( [&count](auto const& l){ ChefTest_ASSERT_EQUAL(l,"abc"); ++count; });
    ChefTest_ASSERT( count == 1 );
}

ChefTest_TEST_CASE( "begin/end" )
{
    using Either = Either<int,std::string>;

    Either left = Either::Left(12);
    Either right = Either::Right("abc");

    bool isCalled = false;
    for( auto& x: left )
    {
        (void)x;
        isCalled = true;
    }
    ChefTest_ASSERT( !isCalled );

    for( auto& x: right )
    {
        ChefTest_ASSERT( x == right.getRight() );
        isCalled = true;
    }
    ChefTest_ASSERT( isCalled );
}

ChefTest_TEST_CASE( "comparison" )
{
    using Either = Either<int,std::string>;

    Either left = Either::Left(12);
    Either right = Either::Right("abc");

    ChefTest_ASSERT( left != right );
    ChefTest_ASSERT( left == left );
    ChefTest_ASSERT( right == right );
    ChefTest_ASSERT( left == Either::Left(12));
    ChefTest_ASSERT( left != Either::Left(13));
    ChefTest_ASSERT( right == Either::Right("abc") );
    ChefTest_ASSERT( right != Either::Right("abcd") );
}

ChefTest_TEST_CASE( "mapMove" )
{
    struct Movable
    {
        Movable()
            : m_moveConstructed{false}
            , m_moveAssigned{false}
        {}

        Movable( Movable&& ) noexcept
            : m_moveConstructed{true}
            , m_moveAssigned{false}
        {}

        Movable& operator=( Movable&& ) noexcept
        {
            m_moveAssigned = true;
            return *this;
        }

        Movable( Movable const& ) = delete;
        Movable& operator=( Movable const& ) = delete;


        bool m_moveConstructed;
        bool m_moveAssigned;
    };

    Either<bool,Movable> source = Either<bool,Movable>::Right( Movable{} );
    auto result = std::move(source).mapMove( []( auto&& x ) { return std::move(x); });
    ChefTest_ASSERT( result.isRight() );
    ChefTest_ASSERT( result.getRight().m_moveConstructed );
    ChefTest_ASSERT( !result.getRight().m_moveAssigned );
}

ChefTest_TEST_CASE( "Pattern matching - functional" )
{
    using Either = Either<int,std::string>;
    Either a = Either::Right( "abc" );

    int result0 = a
        .matchRight( []( auto&& s ) { return (int)s.size(); } )
        .matchLeft( []( auto&& n ){ return n*3; } );

    ChefTest_ASSERT_EQUAL( result0, 3 );

    Either b = Either::Left( 10 );
    int result1 = b
        .matchRight( []( auto&& s ) { return (int)s.size(); } )
        .matchLeft( []( auto&& n ){ return n*3; } );

    ChefTest_ASSERT_EQUAL( result1, 30 );
}

ChefTest_TEST_CASE( "Pattern matching - imperative" )
{
    using Either = Either<int,std::string>;
    Either a = Either::Right( "abc" );

    int result0{};
    a
        .matchRight( [&result0]( auto&& s ) { result0 = s.size(); } )
        .matchLeft( [&result0]( auto&& n ){ result0 = n*3; } );

    ChefTest_ASSERT_EQUAL( result0, 3 );

    Either b = Either::Left( 10 );
    int result1{};
    b
        .matchRight( [&result1]( auto&& s ) { result1 = s.size(); } )
        .matchLeft( [&result1]( auto&& n ){ result1 = n*3; } );

    ChefTest_ASSERT_EQUAL( result1, 30 );
}
