/**
 * @file Machine.hh
 * @date 2022-06-03
 * @author Massimiliano Pagani
 * @addtogroup ChefMachine
 * @{
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined(CHEF_MACHINE_HH)
#define CHEF_MACHINE_HH

#include <cstdint>

namespace ChefMachine
{
    /**
     * Retrieves the value of the program counter where the function is called.
     *
     * @return caller program counter.
     */
    uintptr_t getPc() noexcept;

    /**
     * Retrieves the value of the stack pointer.
     *
     * @return actual stack pointer value.
     */
    uintptr_t getSp() noexcept;
}

#include <ChefMachine/Machine_Impl.hh>

#endif//CHEF_MACHINE_HH

/** @} */