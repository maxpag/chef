/**
 * @file ChefFun/Option_void.tcc
 * @author Massimiliano Pagani
 * @addtogroup ChefFun
 * @{
 */

#if !defined( CHEFFUN_OPTION_HH )
#error "This file is not meant to be included directly, use <ChefFun/Option.hh>"
#endif

/**
 * This is the template specialization for Option on type void. void is a bit
 * of irregular type so it can be handled in the Option main template.
 */

template<>
class Option<void>
{
    public:
        /**
         * Constructs a valid Option<void>. Since void has no actual value, the
         * method takes no argument.
         *
         * @return a valid Option<void>
         */
        [[nodiscard]] static Option<void> Some() noexcept;

        /**
         * Constructs an invalid (empty) Option<void>.
         * @return  an invalid Option<void>
         */
        [[nodiscard]] static Option<void> None() noexcept;

        /**
         * Returns true if the option is valid, or false when the option is
         * invalid (empty).
         *
         * @return true when the option is valid, false otherwise.
         * @deprecated use #isDefined() or #isEmpty() instead
         */
        [[deprecated("use isDefined()/isEmpty instead")]]
        [[nodiscard]] bool isValid() const noexcept;

        /**
         * Returns true if the option has a value.
         *
         * @return true if the option has a value.
         */
        [[nodiscard]] bool isDefined() const noexcept;

        /**
         * Returns true if the option has no values.
         *
         * @return true if the option has no values.
         */
        [[nodiscard]] bool isEmpty() const noexcept;

        /**
         * Transforms the Option<void> into another option, possibly with a
         * different type.
         *
         * @tparam F the type of the transforming function, must take no argument.
         * @param f the function.
         * @return a new Option parametrized on the return type of F. If the
         *         option on which map is invoked is valid, then another valid
         *         option is returned, otherwise if the source option is invalid
         *         then an invalid option is returned.
         */
        template<typename F>
        auto map( F f ) const noexcept( noexcept( f())) -> Option<std::invoke_result_t<F>>;

        /** @{
         * @see Option<T>::matchSome()
         * @see Option<T>::matchNone()
         */
        template<typename F>
        Option<std::invoke_result_t<F>> matchSome( F f ) const noexcept( noexcept( f() ));

        template<typename F>
        void matchNone( F f ) const noexcept( noexcept( f() ) );

        /** @} */
    private:
        explicit Option( bool isValid ) noexcept;

        bool m_isValid;
};

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::: implementation :::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

inline bool
Option<void>::isValid() const noexcept
{
    return m_isValid;
}

inline bool Option<void>::isDefined() const noexcept
{
    return m_isValid;
}
inline bool Option<void>::isEmpty() const noexcept
{
    return !m_isValid;
}

template<typename F> inline
auto Option<void>::map( F f ) const noexcept( noexcept( f())) -> Option<std::invoke_result_t<F>>
{
    typedef std::invoke_result_t<F> TargetType;

    return isDefined() ?
        Option<TargetType>::Some( f() ) :
        Option<TargetType>::None();
}

template<typename F>
inline Option<std::invoke_result_t<F>>
Option<void>::matchSome( F f ) const noexcept( noexcept( f() ))
{
    return map( f );
}

template<typename F> inline
void Option<void>::matchNone( F f ) const noexcept( noexcept( f() ) )
{
    if( isEmpty() )
    {
        f();
    }
}

inline
Option<void>::Option( bool isValid ) noexcept
    : m_isValid( isValid )
{}

inline Option<void> Option<void>::Some() noexcept
{
    return Option{ true };
}

inline Option<void> Option<void>::None() noexcept
{
    return Option{ false };
}


/** @} */