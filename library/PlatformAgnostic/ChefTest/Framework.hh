/**
 * @addtogroup ChefTest
 * @{
 * @file PlatformAgnostic/ChefTest/Framework.hh
 * @author Massimiliano Pagani
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined( CHEFTEST_CHEFTESTFRAMEWORK_HH )
#  error This file is expected to be included just once in your UnitTest
#else
#  define CHEFTEST_CHEFTESTFRAMEWORK_HH
#endif


#include <vector>
#include <string>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <sstream>
#include <chrono>
#include <variant>
#include <memory>
#include <map>
#include <ChefFun/Either.hh>
#include <numeric>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcomment"
/**
 * This is a single header Unit Test Framework for C++. By including this header
 * in your Unit Test files you will get the advantage of structured reporting
 * along with a couple of testing tool.
 * 
 * Usually your C++ unit test file will begin with a few instructions to let the
 * file compiles itself. This is something like:
 * 
 * @code
 *   # ⁄*
 *   g++ -Wall -std=gnu++17 ...
 *   a.out "$@"
 * @endcode
 * 
 * @note in the code above the slash in the comment open has been made with a
 *       Unicode character that is not the ANSI slash, in order to avoid a
 *       warning during compilation. If you copy and paste this code, make sure
 *       to change that character into a straight '/'.
 * 
 * Note that it is important that you select (at least) the C++17 standard and
 * that when you invoke the compiled program you pass the "$@" on the command
 * line. This notation transfers the arguments on the command line of the self
 * compiling code to the command line of the compiled code.
 * 
 * In the source file of the Unit Test start by declaring the name of the suite
 * you are going to test. This is done by using the macro ChefTest_TEST_SUITE:
 * 
 * @code
 *   ChefTest_TEST_SUITE( "A smart test suite name" )
 * @endcode
 * 
 * Ending semicolon is not required, but doesn't hurt if you add it nonetheless.
 * 
 * This name is used to identify this specific compilation unit when more unit
 * tests are performed at the same time.
 * 
 * Each test can be designed as a function. Rather than declaring a conventional
 * function you need to use the macro ChefTest_TEST_CASE as in the following
 * code:
 * 
 * @code
 *   ChefTest_TEST_CASE( "Checking obvious" )
 *   {
 *       int x = 2+2;
 *       ChefTest_ASSERT( x == 4 );
 *   }
 * @endcode
 * 
 * The test case macro is followed by a function body. Ideally all the test code
 * for this case should stay here, but you can call subfunctions declared in the
 * traditional way:
 * 
 * @code
 *   int double( int x )
 *   {
 *       int result = x*2;
 *       ChefTest_ASSERT( result > 0 );
 *       return result;
 *   }
 * 
 *   ChefTest_TEST_CASE( "Checking obvious" )
 *   {
 *       int x = double( 2 );
 *       ChefTest_ASSERT( x == 4 );
 *   }
 * @endcode
 * 
 * ChefTest_ASSERT macro checks the conditions of the test. It works in the same
 * way the standard assert macro (#include <cassert>) works.
 * 
 * The test succeeds if none of the ChefTest_ASSERT in the unit test code has an
 * argument that evaluates to false.
 * 
 * Also the ChefTest_TEST_CASE is interrupted at the first failed
 * ChefTest_ASSERT. So provide some setup code at each ChefTest_TEST_CASE head.
 * 
 * You can optionally use two additional macros: #ChefTest_TEST_SETUP() and
 * #ChefTest_TEST_CLEANUP(). They have to be instantiated and followed by a body
 * like a function. Such as:
 * 
 * @code
 *   ChefTest_TEST_SETUP()
 *   {
 *       initializeEnviroment();
 *       g_check = 0;
 *   }
 * @endcode
 * 
 * This code, if present is executed before the first test case. The purpose is
 * to perform common initialization/setup.
 * 
 * The same holds for #ChefTest_TEST_CLEANUP() optionally defining code that is
 * executed after the last test case.
 * 
 * There test framework supports three outputs:
 * <ul>
 *  <li> text brief
 *  <li> text verbose
 *  <li> line oriented
 * </ul>
 * 
 * Text output formats are expected to be read by humans, while the line 
 * oriented output is for integration with mambo build system. The latter may
 * be turned in a SureFire compatible XML test result by the mambo system.
 * 
 * In order to select one of the output format you have to invoke the test in
 * the following way:
 * 
 * @code
 *   . ChefTest/UnitTestSource.cc --format=<type>
 * @endcode
 * 
 * or
 * 
 * @code
 *   . ChefTest/UnitTestSource.cc -f <type>
 * @endcode
 * 
 * Where type can be one of the following:
 * <ul>
 *  <li> brief
 *  <li> verbose
 *  <li> line
 * </ul>
 * 
 * No other options are supported.
 */
#pragma GCC diagnostic pop

namespace ChefTest
{
    /**
     * Reporter is the abstract base class for reporting about Unit Tests.
     * Heirs must define virtual methods to receive test events.
     */
    class Reporter
    {
        public:
            explicit Reporter( char const* title );
            virtual ~Reporter() = default;

            void startSuite( char const* suiteName, char const* filename );
            void endSuite( char const* suiteName );
            void startSection( char const* sectionName );
            void endSection();

            void reportFailed( std::string&& message,
                               int line,
                               char const* file );
            void reportPassed();

        private:
            virtual void onSectionStart( char const* sectionName ) = 0;
            virtual void onSectionEnd( int succeeded,
                                       int failed,
                                       double time ) = 0;
            virtual void onSuiteStart( char const* name,
                                       char const* filename ) = 0;
            virtual void onSuiteEnd( char const* name ) = 0;
            virtual void onTestPass() = 0;
            virtual void onTestFail( std::string&& message,
                                     int line,
                                     char const* file ) = 0;
            virtual void onTestEnd() = 0;

            int m_passed;
            int m_failed;
            std::chrono::steady_clock::time_point m_timeStart;
    };

    class TextReporter : public Reporter
    {
        public:
            enum Verbosity
            {
                Brief,
                Detailed
            };
            TextReporter( char const* title, Verbosity verbosity );

        private:
            void onSectionStart( char const* sectionName ) override;
            void onSectionEnd( int succeeded, int failed, double time ) override;
            void onSuiteStart( char const* name,
                               char const* filename ) override;
            void onSuiteEnd( char const* name ) override;
            void onTestPass() override;
            void onTestFail( std::string&& message,
                             int line,
                             char const* file ) override;
            void onTestEnd() override;

            Verbosity m_verbosity;

            static constexpr char BrightWhite[] = { 0x1b, 0x5b, 0x33, 0x37, 0x6d, 0x00 };
            static constexpr char Normal[] = { 0x1b, 0x28, 0x42, 0x1b, 0x5b, 0x6d, 0x00 };
            static constexpr char ColorRed[] = { 0x1b, 0x5b, 0x33, 0x31, 0x6d, 0x00 };
            static constexpr char ColorGreen[] = { 0x1b, 0x5b, 0x33, 0x32, 0x6d, 0x00 };
    };

    class LineReporter : public Reporter
    {
        public:
            explicit LineReporter( char const* title );

        private:
            char const* m_sectionName{};

            void
            onSectionStart( char const* sectionName ) override;
            void onSectionEnd( int succeeded, int failed, double time ) override;
            void onSuiteStart( char const* name,
                               char const* filename ) override;
            void onSuiteEnd( char const* name ) override;
            void onTestPass() override;
            void onTestFail( std::string&& message,
                             int line,
                             char const* file ) override;
            void onTestEnd() override;

            static void printField( std::ostream& out,
                                    std::string const& fieldName,
                                    std::string const& fieldValue );
            static void printField( std::ostream& out,
                                    std::string const& fieldName,
                                    std::map<std::string,std::string> const& attributes );
            static void printField( std::ostream& out,
                                    std::string const& fieldName,
                                    std::string const& attributeName,
                                    std::string const& attributeValue );
    };

    class XmlReporter final : public Reporter
    {
        public:
            explicit XmlReporter( char const* title );

        private:
            using Timestamp = std::chrono::time_point<std::chrono::system_clock>;

            void onSectionStart( char const* sectionName ) final;
            void onSectionEnd( int succeeded, int failed, double time ) final;
            void onSuiteStart( char const* name,
                               char const* filename ) final;
            void onSuiteEnd( char const* name ) final;
            void onTestPass() final;
            void onTestFail( std::string&& message,
                             int line,
                             char const* file ) final;
            void onTestEnd() final;

            std::ostream& indented( std::ostream& out ) const;

            void indent() noexcept;
            void unindent() noexcept;

            static Timestamp now() noexcept;
            static double elapsedSeconds( Timestamp end, Timestamp start ) noexcept;
            template<typename T>
            static std::string makeAttribute( char const* name, T&& value );
            static std::string makeId( char const* name );
            unsigned int countTests();
            unsigned int countFails();
            void printFailures( std::vector<std::string> const& vector );

            static std::string xmlify( char const* text );
            static std::string xmlify( std::string const& text );
            template<typename T>
            static std::string xmlify( T text );

            static constexpr unsigned IndentWidth = 2;
            unsigned m_indent;

            std::string m_testSuiteName;
            struct Test
            {
                std::string name{};
                double time{};
                unsigned testCount{};
                std::vector<std::string> failures{};
                double elapsedSeconds{};
            };
            std::vector<Test> m_unitTests;
            Timestamp m_startTime;
            Timestamp m_endTime;
            std::string m_suiteName;
            std::string m_fileName;
            Test m_currentUnit;

    };

    class TestCase
    {
        public:
            explicit TestCase( char const* name ) noexcept;
            ~TestCase() = default;

            bool run( Reporter& reporter, char const* filename );

            void assertTrue( bool condition,
                             char const* expression,
                             int line,
                             char const* file );

            template<typename T,typename U>
            void assertEqual( T const& lhs,
                              U const& rhs,
                              char const* leftExpression,
                              char const* rightExpression,
                              int line,
                              char const* file );
        private:
            virtual void testCase() = 0;

            char const* m_testName;
            Reporter* m_reporter;
            bool m_testPassed{};
    };

    class TestFunction
    {
        private:
            virtual void testFunction() = 0;
        public:
            void run();
    };

    /**
     * The test suite. A test suite is a collection of tests.
     */
    class TestSuite
    {
        public:
            /**
             * Constructs a test suite given the name.
             * 
             * @param name the name of the test suite.
             */
            TestSuite( char const* name, char const* filename ) noexcept;
            ~TestSuite() = default;

            /**
             * Adds a test to the suite.
             * 
             * @param test a test case.
             */
            void addTest( TestCase* test );

            /**
             * Add a function for setting up or cleaning up the test suite. Use
             * #ChefTest_TEST_SETUP() and #ChefTest_TEST_CLEANUP() for defining
             * your code.
             * 
             * Setup code, if defined, is executed just once before all the
             * tests. Cleanup code, if defined, is executed just once after all
             * the tests have run. Note that cleanup is executed even if one or 
             * more test cases fail.
             * 
             * @param function a pointer to the TestFunction class which defines
             *                 the code to execute.
             * @{
             * 
             */
            [[maybe_unused]] void addCleanup( TestFunction* cleanup );
            [[maybe_unused]] void addSetup( TestFunction* setup );
            /** @} */

            /**
             * Executes all the tests.
             * 
             * @param reporter for presenting test results.
             * @return true if all test passed.
             */
            bool run( Reporter& reporter );

            [[nodiscard]] char const* getTitle() const;

            void assertTrue( bool condition,
                             char const* expression,
                             int line,
                             char const* file );

            template<typename T, typename U>
            void assertEqual( T const& lhs,
                              U const& rhs,
                              char const* leftExpression,
                              char const* rightExpression,
                              int line,
                              char const* file );

        private:
            void runSetup();
            void runCleanup();

            char const* m_name;
            char const* m_filename;
            std::vector<TestCase*> m_tests;
            TestCase* m_currentTest = nullptr;
            TestFunction* m_setupCode = nullptr;
            TestFunction* m_cleanupCode = nullptr;
    };

    // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // :::::::::::::::::::::::::::::::::::::::::: TestSuite implementation :::
    // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    TestSuite::TestSuite( char const* name, char const* filename ) noexcept :
               m_name{ name },
               m_filename{ filename }
    {}

    void
    TestSuite::addSetup( TestFunction* setup )
    {
        m_setupCode = setup;
    }

    void
    TestSuite::addCleanup( TestFunction* cleanup )
    {
        m_cleanupCode = cleanup;
    }
    
    void
    TestSuite::addTest( TestCase* test )
    {
        m_tests.push_back( test );
    }

    void TestSuite::runSetup()
    {
        if( m_setupCode != nullptr )
        {
            m_setupCode->run();
        }
    }

    void TestSuite::runCleanup()
    {
        if( m_cleanupCode != nullptr )
        {
            m_cleanupCode->run();
        }
    }

    bool TestSuite::run( Reporter& reporter  )
    {
        runSetup();
        reporter.startSuite( m_name, m_filename );
        bool ok = true;
        for( auto test : m_tests )
        {
            m_currentTest = test;
            bool current = test->run( reporter, m_filename );
            ok = ok && current;
        }
        m_currentTest = nullptr;
        reporter.endSuite( m_name );
        runCleanup();
        return ok;
    }

    char const* TestSuite::getTitle() const
    {
        return m_name;
    }

    void 
    TestSuite::assertTrue( bool condition,
                           char const* expression,
                           int line,
                           char const* file )
    {
        assert( m_currentTest != nullptr );
        m_currentTest->assertTrue( condition, expression, line, file );
    }

    template<typename T, typename U>
    void
    TestSuite::assertEqual( T const& lhs,
                            U const& rhs,
                            char const* leftExpression,
                            char const* rightExpression,
                            int line,
                            char const* file )
    {
        assert( m_currentTest != nullptr );
        m_currentTest->assertEqual( lhs,
                                    rhs,
                                    leftExpression,
                                    rightExpression,
                                    line,
                                    file );
    }

    // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::: TestCase implementation :::
    // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    TestCase::TestCase( char const* name ) noexcept :
              m_testName{name},
              m_reporter{nullptr},
              m_testPassed{false}
    {}

    bool TestCase::run( Reporter& reporter, char const* filename )
    {
        m_reporter = &reporter;
        reporter.startSection( m_testName );
        m_testPassed = true;
        try
        {
            testCase();
        }
        catch( ... )
        {
            reporter.reportFailed( "unhandled exception", 0, filename );
            m_testPassed = false;
        }
        reporter.endSection();
        return m_testPassed;
    }

    void TestCase::assertTrue( bool condition,
                               char const* expression,
                               int line,
                               char const* file )
    {
        if( condition )
        {
            m_reporter->reportPassed();
        }
        else
        {
            using namespace std::string_literals;
            std::string message{ "'"s+expression+"' is not true"s };
            m_reporter->reportFailed( std::move(message), line, file );
            m_testPassed = false;
        }
    }

    template<class X, class Y, class Op>
    struct op_valid_impl
    {
        template<class U, class L, class R>
        static auto test(int) -> decltype(std::declval<U>()(std::declval<L>(), std::declval<R>()),
            void(), std::true_type());

        template<class U, class L, class R>
        static auto test(...) -> std::false_type;

        using type = decltype(test<Op, X, Y>(0));

    };

    template<class X, class Y, class Op> using op_valid = typename op_valid_impl<X, Y, Op>::type;

    namespace notstd {

        struct left_shift {
            template <class L, class R>
            constexpr auto operator()(L&& l, R&& r) const noexcept(noexcept(std::forward<L>(l) << std::forward<R>(r)))
                -> decltype(std::forward<L>(l) << std::forward<R>(r))
            {
                return std::forward<L>(l) << std::forward<R>(r);
            }
        };
    }

    template<class X, class Y> using has_left_shift = op_valid<X, Y, notstd::left_shift>;

    template<typename T, typename U>
    void TestCase::assertEqual( T const& lhs,
                                U const& rhs,
                                char const* leftExpression,
                                char const* rightExpression,
                                int line,
                                char const* file )
    {
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wsign-compare"
        bool equals = (lhs == rhs);
        #pragma GCC diagnostic pop

        if( equals )
        {
            m_reporter->reportPassed();
        }
        else
        {
            using namespace std::string_literals;
            std::string message{
                "'"s+leftExpression+"' != '"s+rightExpression+"'"s
            };
            if constexpr (
                has_left_shift<std::ostream,T>() &&
                has_left_shift<std::ostream,U>()
                )
            {
                std::ostringstream detailedMessage;
                detailedMessage << " ('" << lhs << "' != '" << rhs << "')";
                message += detailedMessage.str();
            }
            m_reporter->reportFailed( std::move(message), line, file );
            m_testPassed = false;
        }
    }

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::: TestFunction Implementation :::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

void
TestFunction::run()
{
    testFunction();
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::: Reporter Implementation :::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    Reporter::Reporter( char const* ) :
              m_passed{0},
              m_failed{0}
    {
    }

    inline void
    Reporter::startSuite( char const* suiteName, char const* filename )
    {
        onSuiteStart( suiteName, filename );
    }

    inline void Reporter::endSuite( char const* suiteName )
    {
        onSuiteEnd( suiteName );
    }

    inline void Reporter::startSection( char const* sectionName )
    {
        m_timeStart = std::chrono::steady_clock::now();
        m_passed = 0;
        m_failed = 0;
        onSectionStart( sectionName );
    }

    inline void Reporter::endSection()
    {
        auto end = std::chrono::steady_clock::now();
        auto millis = std::chrono::duration_cast<std::chrono::milliseconds>( end - m_timeStart );
        std::chrono::duration<double> time = end - m_timeStart;
        onSectionEnd( m_passed, m_failed, time.count() );
    }

    void
    Reporter::reportFailed( std::string&& message, int line, char const* file )
    {
        m_failed += 1;
        onTestFail( std::move(message), line, file );
    }

    void Reporter::reportPassed()
    {
        m_passed += 1;
        onTestPass();
    }

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::: TextReporter Implementation :::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    TextReporter::TextReporter( char const* title, Verbosity verbosity ) :
                  Reporter{ title },
                  m_verbosity{ verbosity }
    {}

    void TextReporter::onSectionStart( char const* sectionName )
    {
        std::cout << BrightWhite << sectionName << Normal << "\n";
    }

    void TextReporter::onSectionEnd( int succeeded, int failed, double )
    {
        std::cout << "  passed: ";
        if( failed == 0 )
        {
            std::cout << ColorGreen;
        }
        std::cout << succeeded << Normal << "\n";
        std::cout << "  failed: ";
        if( failed != 0 )
        {
            std::cout << ColorRed;
        }
        std::cout << failed << Normal << "\n";
    }

    void TextReporter::onSuiteStart( char const* name, char const* )
    {
        std::cout << "== " << name << " ==\n";
    }

    void TextReporter::onSuiteEnd( char const* )
    {
    }

    void TextReporter::onTestPass()
    {
    }

    void
    TextReporter::onTestFail( std::string&& message,
                              int line,
                              char const* file )
    {
        std::cout << file << ":" << line << ": Test failed: " << message << "\n";
    }

    void TextReporter::onTestEnd()
    {
    }

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// :::::::::::::::::::::::::::::::::::::::::::: LineReporter Implementation :::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    LineReporter::LineReporter( char const* title ) :
                  Reporter{ title }
    {}

    void LineReporter::onSuiteStart( char const* name, char const* filename )
    {
        printField( std::cout, "TestSuite", name );
        printField( std::cout, "Property", "filename", filename );
        printField( std::cout, "Property", "date", __DATE__ );
        printField( std::cout, "Property", "time", __TIME__ );
        std::ostringstream builder;
        builder << __GNUC__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__;
        printField( std::cout, "Property", "gcc-version", builder.str() );
    }

    void LineReporter::onSuiteEnd( char const* )
    {
    }

    void LineReporter::onSectionStart( char const* sectionName )
    {
        printField( std::cout, "TestCase", sectionName );
        m_sectionName = sectionName;
    }

    void LineReporter::onSectionEnd( int succeeded, int, double time )
    {
        if( succeeded != 0 )
        {
            std::map<std::string,std::string> attributes =
            {
                {"test", m_sectionName},
                {"time", std::to_string( time )}
            };

            printField( std::cout, "TestPass", attributes );
        }
    }

    void LineReporter::onTestPass()
    {
    }

    void
    LineReporter::onTestFail( std::string&& message,
                              int line,
                              char const* file )
    {
        std::map<std::string,std::string> attributes = {
            {"test", m_sectionName },
            {"file", file },
            {"line", std::to_string(line) },
            {"message", message }
        };

        printField( std::cout, "TestFail", attributes );
    }

    void LineReporter::onTestEnd()
    {
    }

    namespace
    {
        namespace ChefTestEnum
        {
            enum ScanState
            {
                Scan,
                Ampersand
            };

            ScanState stateScan( char c, std::string& result )
            {
                switch( c )
                {
                    case '&':
                        result.push_back( c );
                        return Ampersand;
                    case '#':
                        result.append( "&#34;" );
                        break;
                    case '"':
                        result.append( "&#33;" );
                        break;
                    default:
                        result.push_back( c );
                        break;
                }
                return Scan;
            }

            ScanState stateAmpersand( char c, std::string& result )
            {
                if( c == '"' ) {
                    result.append("&#34;");
                }
                else
                {
                    result.push_back( c );
                }
                return Scan;
            }
        }

        std::string escapeHashes( std::string const& text )
        {
            std::string result;
            result.reserve( text.length() );

            using namespace ChefTestEnum;

            ScanState state = Scan;
            for( char c : text )
            {
                switch( state )
                {
                    case Scan:
                        state = stateScan( c, result );
                        break;
                    case Ampersand:
                        state = stateAmpersand( c, result );
                        break;
                }
            }
            return result;
        }
    }

    void 
    LineReporter::printField( std::ostream& out,
                              std::string const& fieldName,
                              std::string const& fieldValue )
    {
        out << escapeHashes( fieldName ) << ": " 
            << escapeHashes( fieldValue ) << '\n';
    }

    void
    LineReporter::printField( std::ostream& out,
                              std::string const& fieldName,
                              std::map<std::string,std::string> const& attributes )
    {
        out << escapeHashes( fieldName ) << ":";
        for( auto const& attribute : attributes )
        {
            out << ' ' << escapeHashes( attribute.first ) 
                << "=\"" << escapeHashes( attribute.second ) << "\"";
        }
        out << '\n';
    }

    void
    LineReporter::printField( std::ostream& out,
                              std::string const& fieldName,
                              std::string const& attributeName,
                              std::string const& attributeValue )
    {
        out << escapeHashes( fieldName )
            << ": name=" << escapeHashes( attributeName )
            << " value=\"" << escapeHashes( attributeValue ) << "\"\n";
    }

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// :::::::::::::::::::::::::::::::::::::::::::: XmlReporter implementation :::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    XmlReporter::XmlReporter( char const* title )
        : Reporter{title}
        , m_indent{0}
    {
    }

    std::ostream& XmlReporter::indented( std::ostream& out ) const
    {
        for( unsigned i=0; i<m_indent; ++i )
        {
            out << ' ';
        }
        return out;
    }

    inline void XmlReporter::indent() noexcept
    {
        m_indent += IndentWidth;
    }

    inline void XmlReporter::unindent() noexcept
    {
        if( m_indent >= IndentWidth )
        {
            m_indent -= IndentWidth;
        }
    }

    inline XmlReporter::Timestamp XmlReporter::now() noexcept
    {
        return std::chrono::system_clock::now();
    }

    inline double XmlReporter::elapsedSeconds( Timestamp end, Timestamp start ) noexcept
    {
        // NOLINTNEXTLINE(cppcoreguidelines-narrowing-conversions)
        return std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    }


    void XmlReporter::onSectionStart( char const* sectionName )
    {
        Test empty;
        std::swap( empty, m_currentUnit );

        m_currentUnit.name = sectionName;
    }

    void XmlReporter::onSectionEnd( int succeeded, int failed, double time )
    {
        m_currentUnit.time = time;
        m_currentUnit.testCount = succeeded+failed;
        m_unitTests.push_back( m_currentUnit );
    }

    void XmlReporter::onTestPass()
    {
    }

    void XmlReporter::onTestFail( std::string&& message, int line, char const* file )
    {
        std::ostringstream out;
        out << file << ':' << line << ": " << message;
        m_currentUnit.failures.push_back( out.str() );
    }

    void XmlReporter::onTestEnd()
    {
    }

    void XmlReporter::onSuiteStart( char const* name, char const* filename )
    {
        m_suiteName = name;
        m_fileName = filename;
        m_startTime = now();
    }

    std::string XmlReporter::xmlify( std::string const& text )
    {
        std::string xml{};
        xml.reserve( text.size() );
        for( char ch : text )
        {
            switch( ch )
            {
                case '"': xml.append( "&quot;" ); break;
                case '\'': xml.append( "&apos;" ); break;
                case '&': xml.append( "&amp;" ); break;
                case '<': xml.append( "&lt;" ); break;
                case '>': xml.append( "&gt;" ); break;
                default: xml.append( 1, ch ); break;
            }
        }
        return xml;
    }

    inline std::string XmlReporter::xmlify( char const* text )
    {
        return xmlify( std::string{text} );
    }

    template<typename T>
    std::string XmlReporter::xmlify( T text )
    {
        return std::to_string( T{text} );
    }

    template<typename T>
    std::string XmlReporter::makeAttribute( char const* name, T&& value )
    {
        std::ostringstream out;
        out << name << "=\"" << xmlify( value ) << "\"";
        return out.str();
    }

    std::string XmlReporter::makeId( char const* name )
    {
        std::string id{};
        while( *name != '\0' )
        {
            id.append( 1, std::isalnum( *name ) ? *name : '.' );
            ++name;
        }
        return id;
    }

    unsigned XmlReporter::countTests()
    {
        return std::accumulate(
            m_unitTests.begin(),
            m_unitTests.end(),
            0,
            []( int count, auto const& unit ){
                return count + unit.testCount;
            }
        );
    }

    unsigned XmlReporter::countFails()
    {
        return std::accumulate(
            m_unitTests.begin(),
            m_unitTests.end(),
            0,
            []( int count, auto const& unit ){
                return count + unit.failures.size();
            }
        );
    }

    void XmlReporter::onSuiteEnd( char const* name )
    {
        m_endTime = now();
        indented( std::cout ) << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
        indented( std::cout ) << "<testsuites "
                              << makeAttribute( "id", makeId( __DATE__ __TIME__ ) ) << " "
                              << makeAttribute( "name", name ) << " "
                              << makeAttribute( "tests", countTests() ) << " "
                              << makeAttribute( "failures", countFails() ) << " "
                              << makeAttribute( "time", elapsedSeconds( m_endTime, m_startTime ))
                              << ">\n";
        indent();
        indented( std::cout ) << "<testsuite "
                              << makeAttribute( "id", makeId( name ) ) << " "
                              << makeAttribute( "name", name ) << " "
                              << makeAttribute( "tests", countTests() ) << " "
                              << makeAttribute( "failures", countFails() ) << " "
                              << makeAttribute( "time", elapsedSeconds( m_endTime, m_startTime ))
                              << ">\n";
        indent();
        for( auto const& unit : m_unitTests )
        {
            indented( std::cout ) << "<testcase "
                                  << makeAttribute( "id", unit.name ) << " "
                                  << makeAttribute( "name", unit.name ) << " "
                                  << makeAttribute( "time", unit.elapsedSeconds )
                                  << ">\n";
            if( !unit.failures.empty() )
            {
                indent();
                    printFailures( unit.failures );
                unindent();
            }
            indented( std::cout ) << "</testcase>\n";
        }
        unindent();
        indented( std::cout ) << "</testsuite>\n";
        unindent();
        indented( std::cout ) << "</testsuites>\n";
    }

    void XmlReporter::printFailures( std::vector<std::string> const& vector )
    {
        for( auto const& failure : vector )
        {
            indented( std::cout ) << "<failure "
                                  << makeAttribute( "message", failure ) << " "
                                  << makeAttribute( "type", "SEVERE")
                                  << ">\n";
            indent();
            indented( std::cout ) << xmlify( failure ) << "\n";
            unindent();
            indented( std::cout ) << "</failure>\n";
        }
    }


// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::: misc implementation :::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    constexpr char const* DefaultFormat = "brief";

    void usage( char const* argv0 )
    {
        std::cout <<
            "Usage:\n"
            "  " << argv0 << " [-f|--format=<format>]\n"
            "\n"
            "Where:\n"
            "  <format> is the output format. One choosen from the following\n"
            "           list:\n"
            "           * brief - brief text report\n"
            "           * verbose - verbose text report\n"
            "           * line - surefire XML format\n"
            "           Default is " << DefaultFormat << "\n"
        ;
    }

    class Options
    {
        public:
            typedef ChefFun::Either<std::string,Options> ParseResult;
            static ParseResult parseArgs(  int argc, char** argv );
            enum class FormatType
            {
                Brief,
                Verbose,
                Line,
                Xml
            };

            enum class RunMode
            {
                CompileOnly,
                PerformTests
            };

            [[nodiscard]] FormatType getFormatType() const;
            [[nodiscard]] RunMode getRunMode() const;
        private:
            Options( FormatType type, RunMode runMode );

            static ChefFun::Either<std::string,FormatType>
            decodeFormat( std::string const& format );

            FormatType m_type;
            RunMode m_runMode;
    };

    Options::Options( Options::FormatType type, RunMode runMode )
        : m_type{ type }
        , m_runMode{ runMode }
    {}

    inline Options::FormatType Options::getFormatType() const
    {
        return m_type;
    }

    inline Options::RunMode Options::getRunMode() const
    {
        return m_runMode;
    }

    ChefFun::Either<std::string,Options::FormatType>
    Options::decodeFormat( std::string const& format )
    {
        using namespace ChefFun;
        using Either=Either<std::string,Options::FormatType>;

        auto type = (format == "brief") ?
                        Either::Right(FormatType::Brief) :
                    (format == "verbose") ?
                        Either::Right(FormatType::Verbose) :
                    (format == "line") ?
                        Either::Right(FormatType::Line) :
                    (format == "xml") ?
                        Either::Right(FormatType::Xml) :
                    Either::Left( "Unknown format '" + format + "'" );
        return type;
    }

    Options::ParseResult
    Options::parseArgs( int argc, char** argv )
    {
        using namespace ChefFun;
        using namespace std::string_literals;
        using Either=Either<std::string,Options::FormatType>;
        auto formatType = Either::Right( FormatType::Brief );
        RunMode runMode = RunMode::PerformTests;
        --argc;
        ++argv;
        while( argc != 0 )
        {
            if( argv[0] == "-f"s )
            {
                if( argc <= 1 )
                {
                    return ParseResult::Left( "Missing argument for switch '-f'"s );
                }
                formatType = decodeFormat( argv[1] );
            }
            else if( constexpr char Prefix[] = "--format="; std::string{ argv[0] }.find( Prefix ) == 0 )
            {
                char const* format = argv[0] + sizeof( Prefix ) - 1;
                formatType = decodeFormat( format );
            }
            else if( argv[0] == "--compile-only"s )
            {
                runMode = RunMode::CompileOnly;
            }
            else {
                return ParseResult::Left( "Unknown argument" );
            }
            --argc;
            ++argv;
        }
        return formatType.map( [runMode](auto format){
            return Options{format,runMode};
        });
    }

    std::unique_ptr<Reporter>
    getReporter( char const* title, Options::FormatType type )
    {
        switch( type )
        {
            case Options::FormatType::Brief:
                return std::make_unique<TextReporter>( title,
                                                       TextReporter::Verbosity::Brief );

            case Options::FormatType::Verbose:
                return std::make_unique<TextReporter>( title,
                                                       TextReporter::Verbosity::Detailed );
                 
            case Options::FormatType::Line:
                return std::make_unique<LineReporter>( title );

            case Options::FormatType::Xml:
                return std::make_unique<XmlReporter>(title);

            default:
                assert( false );
        }
    }

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::: Global Variables :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

}

/**
 * Defines the test suite. You need to instantiate this macro, outside any
 * function, at the beginning of your unit test file.
 * 
 * No semicolon is needed after the macro invokation.
 * 
 * @param test suite name (C-string literal)
 */
#define ChefTest_TEST_SUITE(X)   ChefTest::TestSuite g_testSuite{ X, __FILE__ };

#define ChefTest_FUNCTION_IMPL2( L, Op )   \
    class TestFunction_ ## L : public ChefTest::TestFunction                    \
    {                                                                   \
            void testFunction() override;                                   \
        public:                                                         \
            TestFunction_ ## L()                              \
            {                                                           \
                g_testSuite.add ## Op ( this );                            \
            }                                                           \
    } g_testFunction_##L;                                                   \
    void TestFunction_ ## L::testFunction() 

#define ChefTest_FUNCTION_IMPL( L, Op ) ChefTest_FUNCTION_IMPL2( L, Op )

/**
 * Defines code that is executed before the first test case.
 * 
 * Instantiate this macro and write the code in a brace enclosed block (the 
 * same way you would do for a function).
 */

#define ChefTest_TEST_SETUP()   ChefTest_FUNCTION_IMPL( __LINE__, Setup )

/**
 * Defines code that is executed after the last test case.
 * 
 * Instantiate this macro and write the code in a brace enclosed block (the 
 * same way you would do for a function).
 */
#define ChefTest_TEST_CLEANUP() ChefTest_FUNCTION_IMPL( __LINE__, Cleanup )

#define ChefTest_TEST_CASE_IMPL(X,L,F)                                  \
    class TestCase_ ## L : public ChefTest::TestCase                    \
    {                                                                   \
            void testCase() override;                                   \
        public:                                                         \
            TestCase_ ## L() noexcept : TestCase{X}                     \
            {                                                           \
                g_testSuite.addTest( this );                            \
            }                                                           \
    } g_testCase_##L;                                                   \
    void TestCase_ ## L::testCase() 

#define ChefTest_TEST_CASE_IMPL2(X,L,F) ChefTest_TEST_CASE_IMPL( X, L, F )

/**
 * Introduces a test case body. After this macro you can write unit test code in
 * brace delimited code block (as it would be a function body).
 * 
 * Each condition in the test has to be checked with #ChefTest_ASSERT()
 * 
 * @param test case name (C-string literal)
 */
#define ChefTest_TEST_CASE(X)           \
                ChefTest_TEST_CASE_IMPL2( X, __LINE__, __FILE__ )

/**
 * Checks that a condition of the test is true. You can use this macro much like
 * you would use the standard assert macro (found in <cassert> header file).
 * 
 * When the argument of the macro is evaluated to false, the execution of this
 * test case is terminated and the execution continues from the next test case 
 * (if any).
 * 
 * @param X any boolean expression.
 */
#define ChefTest_ASSERT( X )            \
                g_testSuite.assertTrue( (X), #X, __LINE__, __FILE__ )

/**
 * Checks for equality. This macro can be used instead of the basic
 * #ChefTest_ASSERT() to check for equality.
 * 
 * The behaviour is the same as:
 * @code
 *   ChefTest_ASSERT( X == Y );
 * @endcode
 * 
 * @param X the left term of the comparison.
 * @param Y the right term of the comparison.
 */
#define ChefTest_ASSERT_EQUAL( X, Y )   \
                g_testSuite.assertEqual( (X), (Y), #X, #Y, __LINE__, __FILE__ )

int main( int argc, char** argv )
{
    using namespace ChefTest;
    extern TestSuite g_testSuite;

    auto errorOrOptions = Options::parseArgs( argc, argv );
    if( errorOrOptions.isLeft() )
    {
        std::cerr << errorOrOptions.getLeft() << "\n";
        ChefTest::usage( argv[0] );
        return EXIT_FAILURE;
    }
    auto const& options = errorOrOptions.getRight();

    if( options.getRunMode() == Options::RunMode::CompileOnly )
    {
        return EXIT_SUCCESS;
    }
    auto reporter = getReporter( g_testSuite.getTitle(),
                                 options.getFormatType() );

    bool ok = g_testSuite.run( *reporter );

    return ok ? EXIT_SUCCESS : EXIT_FAILURE;
}

///@}
