/**
 * @addtogroup ChefBase
 * @{
 * @file ChefDebug/assert.cc
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2014-05-15
 *
 */

#include <ChefBase/assert.hh>

namespace ChefBase
{
    namespace Assert
    {
        namespace
        {
#if defined( ChefBase_BRIEF_ASSERT )
            void emptyCallback( uintptr_t,  uintptr_t ) noexcept
            {
            }
#elif defined( ChefBase_FULL_ASSERT )
            void emptyCallback( char const*, int, char const* ) noexcept
            {
            }
#else
#  error "either ChefBase_FULL_ASSERT or ChefBase_BRIEF_ASSERT needs to be defined"
#endif
        }

        CallBackFn* g_assertCallback = emptyCallback;

        void setCallback( CallBackFn* callback ) noexcept
        {
            g_assertCallback = callback;
        }

    }
}