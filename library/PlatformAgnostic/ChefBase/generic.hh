/**
 * @addtogroup ChefBase
 * @{
 * @file ChefBase/generic.hh
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2014-10-15
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined( CHEFBASE_GENERIC_HH )
#define CHEFBASE_GENERIC_HH

#include <type_traits>
#include <iterator>

namespace ChefBase
{

    /**
     * The generic namespace includes utilities for generic programming, mostly
     * templates that are used by other generic components provided by the
     * Chef Library.
     */
    namespace generic 
    {

        /**
         * Erase elements from a container. This works only if the container
         * does not invalidate the iterators when erasing elements.
         *
         * @param items container to operate on.
         * @param predicate the predicate to test for deciding whether an
         *                  element has to be erased or not. If the predicate
         *                  returns true then the element is erased.
         * @code
         * // 'container' could be a std::map
         * // 'item_type' is what you might store in your container
         * using generic::erase_if;
         * erase_if(container, []( item_type& item )
         * {
         *    return insert_appropriate_test ;
         * });
         * @endcode
         */
        template< typename ContainerT, typename PredicateT >
        void erase_if( ContainerT& items, const PredicateT& predicate )
        {
            for( auto it = items.begin(); it != items.end(); )
            {
                if( predicate(*it) )
                {
                    it = items.erase(it);
                }
                else
                {
                    ++it;
                }
            }
        }

        template<typename Iter1,typename Iter2>
        bool areRangesEqual( Iter1 scan1, Iter1 end1, Iter2 scan2, Iter2 end2 )
        {
            while( scan1 != end1 && scan2 != end2 )
            {
                if( *scan1 != *scan2 )
                {
                    return false;
                }
                ++scan1;
                ++scan2;
            }
            return scan1 == end1 && scan2 == end2;
        }

        /**
         * Checks if a container has an item.
         *
         * @note this does not work with native array.
         *
         * @tparam C the container class. Can't be native array.
         * @tparam T the type of items contained in C (or any compatible type).
         * @param container the container to search.
         * @param item the item to search for
         * @retval true if the item was found in the container.
         * @retval false if the item was not in the container.
         */
        template<typename C, typename T>
        [[nodiscard]] bool
        contains( C const& container, T const& item ) noexcept
        {
            if( container.empty() )
            {
                return false;
            }
            auto end = std::end( container );

            return std::find( std::begin(container), end, item ) != end;
        }
    }

    /**
     * Defines an uninitialized storage type for a given type. This is a useful
     * device to avoid Unions (that cannot be used for type punning in C++) and
     * provide uninitialized space for an object to be constructed at a later
     * moment.
     *
     * @see as()
     * @see create()
     * @see destroy()
     *
     * @tparam T the type you want memory for.
     */
    template<typename T>
    using MemoryFor = typename std::aligned_storage<sizeof(T),alignof(T)>::type;

    template<typename T>
    inline T* as( MemoryFor<T>& storage) noexcept
    {
        return reinterpret_cast<T*>( &storage );
    }

    template<typename T, typename ...P>
    inline void create( MemoryFor<T>& memory, P&& ... p ) noexcept( noexcept( T{p...} ) )
    {
        new( as<T>(memory) ) T{ (std::forward<P>(p))... };
    }

    template<typename T>
    inline void destroy( MemoryFor<T>& memory ) noexcept
    {
        as<T>(memory)->~T();
    }

    /**
     * std library replacement for std::any_of, std::all_of, std::find_if.
     * Apparently std library templates perform unrolling regardless of
     * compilation options and eat up valuable space.
     */

    template<typename It,typename P>
    bool any_of( It begin, It end, P p ) noexcept;

    template<typename C,typename P>
    bool any_of( C const& container, P p ) noexcept;

    template<typename It,typename P>
    bool all_of( It begin, It end, P p ) noexcept;

    template<typename C,typename P>
    bool all_of( C const& container, P p ) noexcept;

    template<typename It,typename P>
    It find_if( It begin, It end, P p ) noexcept;

    /**
     * A min implementation that allows you to mix different types that can be
     * compared.
     *
     * Note that std C++ version (std::min) fail to compile code like:
     *
     * @code
     * uint8_t a = 3;
     * uint16_t b = 5;
     * auto c = std::min(a,b);
     * @endcode
     *
     * The goal of this piece of code is to overcome this limitation and allow
     * you to use min wherever the old C MIN macro would have worked.
     *
     * @tparam T
     * @param x
     * @return the minimum.
     * @{
     */
    template<typename T>
    inline constexpr auto min( T x )
    {
        return x;
    }

    template<typename T, typename U>
    inline constexpr auto min( T x, U y )
    {
        return  x < y ? x : y;
    }

    template<typename L, typename ...T>
    inline constexpr auto min( L l, T... r )
    {
        return min( l, min( r... ));
    }
    ///@}

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::: template implementations :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename It,typename P>
    bool any_of( It begin, It end, P p ) noexcept
    {
        while( begin != end )
        {
            if( p(*begin) )
            {
                return true;
            }
            ++begin;
        }
        return false;
    }

    template<typename C,typename P>
    inline bool any_of( C const& container, P p ) noexcept
    {
        return ChefBase::any_of( std::begin( container ), std::end( container ), p );
    }

    template<typename It,typename P>
    bool all_of( It begin, It end, P p ) noexcept
    {
        return ! ChefBase::any_of( begin, end, [p]( auto&& x ){ return !p(x); } );
    }

    template<typename C,typename P>
    inline bool all_of( C const& container, P p ) noexcept
    {
        return ChefBase::all_of( std::begin(container), std::end(container), p );
    }

    template<typename It,typename P>
    It find_if( It begin, It end, P p ) noexcept
    {
        while( begin != end )
        {
            if( p(*begin) )
            {
                return begin;
            }
            ++begin;
        }
        return end;
    }

}  // end of namespace ChefBase

#endif
///@}


