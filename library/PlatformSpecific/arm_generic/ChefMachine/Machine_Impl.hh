/**
 * @file Machine_Impl.hh
 * @date 2022-06-03
 * @author Massimiliano Pagani
 * @addtogroup ChefMachine
 * @{
 */

#if !defined(CHEF_MACHINE_IMPL_HH)
#define CHEF_MACHINE_IMPL_HH

#if !defined( CHEF_MACHINE_HH )
#  error "This file is not meant to be included directly, include <ChefMachine/Machine.hh>"
#endif

inline uintptr_t getSp() noexcept
{
    register uint32_t sp asm ("sp");
    return sp;
}

inline uintptr_t getPc() noexcept
{
    asm __current_pc();
}



#endif//CHEF_MACHINE_IMPL_HH
